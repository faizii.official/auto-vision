#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <Servo.h>

Servo servo;
const int signalPin = 13;
const int servoPin = 8;
const int desiredAngle = 5;

const int trigPin1 = 2;
const int echoPin1 = 3;
const int trigPin2 = 4;
const int echoPin2 = 5;
const int ledPin = 7;

const int numReadings = 10;  // Number of readings to average
int readings1[numReadings];  // Array to store distance readings from sensor 1
int readings2[numReadings];  // Array to store distance readings from sensor 2
int index = 0;               // Index of the current reading
int total1 = 0;              // Accumulator for the sum of readings from sensor 1
int total2 = 0;              // Accumulator for the sum of readings from sensor 2

// Set the LCD address
#define LCD_ADDRESS 0x27

// Set the LCD dimensions
#define LCD_COLS 16
#define LCD_ROWS 2

// Create the LCD object
LiquidCrystal_I2C lcd(LCD_ADDRESS, LCD_COLS, LCD_ROWS);

void setup() {
  // Initialize the LCD
  lcd.begin(LCD_COLS, LCD_ROWS);
  lcd.setBacklight(HIGH); // Turn on the backlight (or use HIGH to turn off)

  servo.attach(servoPin);
  pinMode(signalPin, INPUT);
  pinMode(trigPin1, OUTPUT);
  pinMode(echoPin1, INPUT);
  pinMode(trigPin2, OUTPUT);
  pinMode(echoPin2, INPUT);
  pinMode(ledPin, OUTPUT);

  // Initialize the distance readings arrays
  for (int i = 0; i < numReadings; i++) {
    readings1[i] = 0;
    readings2[i] = 0;
  }
}

void loop() {
  int signalValue = digitalRead(signalPin);

  // Read distance from sensor 1
  digitalWrite(trigPin1, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin1, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin1, LOW);
  long duration1 = pulseIn(echoPin1, HIGH);
  int distance1 = duration1 / 2 / 29.1;

  // Read distance from sensor 2
  digitalWrite(trigPin2, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin2, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin2, LOW);
  long duration2 = pulseIn(echoPin2, HIGH);
  int distance2 = duration2 / 2 / 29.1;

  // Remove the oldest readings from the totals
  total1 -= readings1[index];
  total2 -= readings2[index];

  // Add the new readings to the totals
  readings1[index] = distance1;
  readings2[index] = distance2;
  total1 += readings1[index];
  total2 += readings2[index];

  // Move to the next reading index
  index = (index + 1) % numReadings;

  // Calculate the average distances
  int averageDistance1 = total1 / numReadings;
  int averageDistance2 = total2 / numReadings;
  int averageDistance = (averageDistance1 + averageDistance2) / 2;

  if (averageDistance < 400) {
    servo.write(5);
  } else if (signalValue == HIGH) {
    servo.write(90);
  } else if (signalValue == LOW) {
    servo.write(5);
  }

  if (averageDistance < 400) {
    digitalWrite(ledPin, HIGH);
    delay(100);
    digitalWrite(ledPin, LOW);
    delay(100);
  } else {
    digitalWrite(ledPin, LOW);
  }

  int servoAngle = servo.read();

  // Display information on the LCD
  lcd.setCursor(0, 0);

  lcd.print("Dist: ");
  lcd.print(averageDistance);
  lcd.print("cm");

  delay(20);
}
